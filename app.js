'use strict';

let arr = ['1', '2', '3', 'sea', 'user', 23, ['stayInLine ->', '1', 2, 3], 'nice :)',];
publishArray(arr);

function publishArray(arr) {
    document.body.insertAdjacentHTML("afterbegin", `<ul id="content"></ul>`);
    document.getElementById('content').style.cssText =
        'display: flex; font: 20px Sans-serif; color: blue; flex-direction: column; list-style: none;';
    arr.map(function newArray(item) {
        let li = document.createElement('li');
        li.innerHTML = `${item}`;
        return document.getElementById('content').append(li);
    });
}


document.getElementById('content').insertAdjacentHTML('afterbegin', '<p id="timerText">Contents will dissapear in ' +
    '<span id="countdown"></span> seconds!</p>');
document.getElementById('timerText').style.cssText = 'font-size: 28px; color: red;';
document.getElementById('countdown').style.cssText = 'font-size: 32px; font-weight: bold;';

function startTimer(duration, display) {
    let timer = duration, seconds;
    setInterval(function() {
        seconds = parseInt(timer % 100);
        display.textContent = seconds;
        if (--timer < 0) {
            document.getElementById('content').remove();
            document.getElementById('text').remove();
        }
    }, 1000);
}

window.onload = function() {
    let tenSecs = 10,
        display = document.querySelector('#countdown');
    startTimer(tenSecs, display);
};
